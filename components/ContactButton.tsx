import React from 'react';
import { Button, StyleSheet } from 'react-native';
import { Contact } from '../interfaces/interfaces';

interface ContactButtonProps {
    text : string,
    color : string, 
    onTouch : Function,
    accessibilityLabel : string
}

// const ContactButton : React.SFC<ContactButtonProps> = ( 
//     // icon, 
//     text,
//     color, 
//     onTouch,
//     accessibilityLabel
//     ) => {
        
//     return (
//         <Button
//             onPress={onTouch}
//             title={text}
//             color={color}
//             accessibilityLabel={accessibilityLabel}
//         />
//     );
// }

const styles = StyleSheet.create({
    contactButton: {
      height: 36,
      borderRadius: 10,
      backgroundColor: '#777777',
      textTransform: 'uppercase'
    }
  });

// export default ContactButton;