import React from "react";
import {
  View,
  TextInput,
  StyleSheet,
} from "react-native";

interface SearchBarProps {
    typeQuery : Function,
    searchQuery : string
}

const SearchBar : React.FC<SearchBarProps> = ({
    typeQuery,
    searchQuery
    }) => {
    return (
      <View>
        <TextInput
          value={searchQuery}
          onChangeText={() => typeQuery('Hey')}
          style={styles.searchBar}
          placeholder='Search'
        />
      </View>
    );
}

const styles = StyleSheet.create({
  searchBar: {
    marginTop: 10,
    height: 52,
    borderRadius: 10,
    outline: '#000000',
    fontSize: 12
  },
  textContainer: {
    justifyContent: "center",
    alignItems: "center",
    padding: 8,
    flexDirection: 'column'
  }
});

export default SearchBar;