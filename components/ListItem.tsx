import React from "react";
import {
  View,
  Text,
  StyleSheet,
} from "react-native";
import { ListItemProps } from '../interfaces/interfaces';

const ListItem : React.SFC<ListItemProps> = (contact, onPress) => {
  const {
    name,
    email
  } = contact; 

    return (
      <View style={styles.listItem}>
        <View>
        </View>
        <View style={styles.textContainer}>
          <Text style={styles.nameText}>{name}</Text>
          <Text style={styles.emailText}>{email}</Text>
        </View>
        <View>
        </View>
      </View>
    );
  }


const styles = StyleSheet.create({
  listItem: {
    height: 55,
    backgroundColor: '#999999',
    borderRadius: 5
  },
  textContainer: {
    justifyContent: "center",
    alignItems: "center",
    padding: 8,
    flexDirection: 'column'
  },
  nameText: {
    marginBottom: 6,
    paddingLeft: 3,
    color: '#333333',
    fontWeight: 'bold',
    fontSize: 12,
  },
  emailText: {
    color: '#777777',
    fontSize: 8,
  },
  rightElementContainer: {
    justifyContent: "center",
    alignItems: "center",
    flex: 0.4
  },
  rightTextContainer: {
    justifyContent: "center",
    marginRight: 10
  }
});

export default ListItem;