export const contacts = [
    {
        id : '1',
        name : 'Mr X',
        phoneNumber : '+380987654321',
        imagePath : '',
        email : 'abc@a.com'
    },
    {
        id : '2',
        name : 'Jenny',
        phoneNumber : '+380548767512',
        imagePath : '',
        email : 'xyz@a.com'
    },
    {
        id : '3',
        name : 'Bill Gates',
        phoneNumber : '+380543217896',
        imagePath : '',
        email : 'wow@a.com'
    },
];