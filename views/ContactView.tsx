import React from 'react';
import {
  StyleSheet,
  View,
  Text,
} from 'react-native';
// import call from 'react-native-phone-call';
// import ContactButton from '../components/ContactButton';
// import Avatar from '../components/Avatar';

const ContactView : React.FC<any> = ({
  route,
  navigation
}) => {
    const {
      contact, 
      onDeleteContact
    } = route.params;

    const {
      id,
      name, 
      phoneNumber, 
      imagePath
    } = contact;

    const onMakeCall = (phoneNumber : string) : void => {
      const args = {
        phoneNumber,
        prompt: false
      }
      
      // TO DO: alertError
      // call(args).catch(console.error)
    }

    const onContactsListScreen = () : void => {
      navigation.goBack();
    }

    return (
      <View style={styles.main}>
        {/* <Avatar/> */}
        <Text style={styles.label}
        />
        <View style={styles.infoContainer}> 
          <Text style={styles.info}
          />
        </View>
        <Text style={styles.label}
        />
        <View style={styles.infoContainer}>
          <Text style={styles.info}
          />
        </View>
        <View>
          {/* <ContactButton
            text={'call'}
            color={'#888888'} 
            onTouch={() => onMakeCall(phoneNumber)}
            accessibilityLabel={'Make a call'}
          />
          <ContactButton
            text={'call'}
            color={'#888888'} 
            onTouch={() => onDeleteContact(id)}
            accessibilityLabel={'Delete contact'}
          /> */}
        </View>
      </View>
    )
}

const styles = StyleSheet.create({
  main: {
    margin: 10,
  },
  label: {
    fontSize: 8,
    color: '#777777'
  },
  infoContainer: {
    // borderBottomWidth: 1,
    // borderBottomStartRadius: '#777777' 
  },
  info: {
    paddingTop: 5,
    paddingBottom: 5, 
    fontSize: 12,
    color: '#333333'
  }
})

export default ContactView;