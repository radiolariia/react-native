import React, { useState } from 'react';
import {
  StyleSheet,
  View,
  FlatList,
  ListRenderItem
} from 'react-native';
import ListItem from '../components/ListItem';
import { Contact } from '../interfaces/interfaces';
import SearchBar from '../components/SearchBar';

import { contacts } from '../data';


const ContactList : React.FC<any> = ({ navigation }) => {
    const [contactsList, setContactsList] = useState<Array<Contact>>(contacts); 
    const [searchQuery, setSearchQuery] = useState<string>('');

    const onChangeText = (text : string) : void => {
      setSearchQuery(text);
    }

    const onDeleteContact = (id : string) : void => {
      const updatedContacts = contactsList.filter(item => item.id !== id);

      setContactsList(updatedContacts);
    }

    const onContactPress = (contact : Contact, onDeleteContact : Function) : void => {
      navigation.jumpTo('Contact Screen', { contact, onDeleteContact })
    }

    const renderItem : ListRenderItem<Contact>= ({ item }) => {
      return (
      <ListItem 
        {...item}
        onPress={() => onContactPress(item, onDeleteContact)}
      />
      );
    }
    return (
      <View style={styles.main}>
        <SearchBar searchQuery={searchQuery} typeQuery={onChangeText}/>
        <FlatList 
          data={contacts}
          renderItem={renderItem}
          keyExtractor={item => item.id}
        />
      </View>
    )
}

const styles = StyleSheet.create({
  main: {
    margin: 10,
  }
})

export default ContactList;