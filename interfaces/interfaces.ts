export interface Contact {
    id : string,
    name : string,
    phoneNumber : string,
    imagePath : string,
    email : string
}

export interface ContactViewProps extends Contact {
    onDeleteContact : Function
}

export interface ListItemProps extends Contact {
    onPress : Function
}