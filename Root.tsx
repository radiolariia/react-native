/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * Generated with the TypeScript template
 * https://github.com/react-native-community/react-native-template-typescript
 *
 * @format
 */

import React, { useState, useEffect } from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
} from 'react-native';

import {
  Header,
  LearnMoreLinks,
  Colors,
  DebugInstructions,
  ReloadInstructions,
} from 'react-native/Libraries/NewAppScreen';
import Contacts from 'react-native-contacts';
import ContactListView from './views/ContactListView';
import ContactView from './views/ContactView';
import { NavigationContainer } from 'react-navigation';
// import { createMaterialTopTabNavigator } from '@react-navigation/material-top-tabs';

Contacts.getAll((err : string, contacts ) => {
  if (err === 'denied'){
    console.log(err);
  } else {
    return contacts;
  }
})

declare const global: {HermesInternal: null | {}};

// const Tab = createMaterialTopTabNavigator();

const App : React.SFC = () => {
  return (
    // <NavigationContainer>
    //   <Tab.Navigator initialRouteName="Contacts List">
    //     <Tab.Screen
    //       name="ContactListView"
    //       component={ContactListView}
    //     />
    //     <Tab.Screen
    //       name="Contact Screen"
    //       component={ContactView}
    //     />
    //   </Tab.Navigator>
    // </NavigationContainer>
    <ContactListView/>
  );
};

const styles = StyleSheet.create({
  scrollView: {
    backgroundColor: Colors.lighter,
  },
  engine: {
    position: 'absolute',
    right: 0,
  },
  body: {
    backgroundColor: Colors.white,
  },
  sectionContainer: {
    marginTop: 32,
    paddingHorizontal: 24,
  },
  sectionTitle: {
    fontSize: 24,
    fontWeight: '600',
    color: Colors.black,
  },
  sectionDescription: {
    marginTop: 8,
    fontSize: 18,
    fontWeight: '400',
    color: Colors.dark,
  },
  highlight: {
    fontWeight: '700',
  },
  footer: {
    color: Colors.dark,
    fontSize: 12,
    fontWeight: '600',
    padding: 4,
    paddingRight: 12,
    textAlign: 'right',
  },
});

export default App;